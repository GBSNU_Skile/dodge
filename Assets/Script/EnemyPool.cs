﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour {
    public GameObject enemy;
    private List<GameObject> pool = new List<GameObject>();
    private static int capacity = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject GetEnemy()
    {
        foreach(GameObject obj in pool)
        {
            if (!obj.activeInHierarchy) return obj;
        }

        return NewObject();
    }

    public void Clear()
    {
        for(int i = pool.Count - 1; i >=0; i--)
        {
            GameObject obj = pool[i];
            pool.Remove(obj);
            Destroy(obj);
        }

        for(int i = 0; i < capacity; i++)
        {
            NewObject();
        }
    }

    private GameObject NewObject()
    {
        GameObject newObj = Instantiate(enemy);
        newObj.SetActive(false);
        pool.Add(newObj);
        return newObj;
    }
}
