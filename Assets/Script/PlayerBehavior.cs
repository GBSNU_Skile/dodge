﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour {
    public float speed = 10.0f;
    private Vector3 moveVector = new Vector3();
    private Vector3 vpAnchor = new Vector3(0.5f, 0.5f, 0.0f);

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position) - vpAnchor;
        float distance = Mathf.Max(Mathf.Abs(pos.x), Mathf.Abs(pos.y));

        if (distance > 0.5f) {
            //Debug.Log("pos = " + pos + ", distance = " + distance);
            pos.x *= 0.5f / distance;
            pos.y *= 0.5f / distance;

            pos += vpAnchor;

            transform.position = Camera.main.ViewportToWorldPoint(pos);
        }
        else
        {
            moveVector.x = Input.GetAxis("Horizontal");
            moveVector.y = Input.GetAxis("Vertical");
            
            transform.Translate(moveVector * Time.deltaTime * speed);
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        GameManager.Instance.GameOver();
    }
}
