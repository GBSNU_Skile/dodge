﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    private bool isGaming = false;

    public GameObject player;
    public GameObject explosion;
    private Animator animExplosion;

    public Text txtScore;
    public Text txtGameOver;

    private float score = 0.0f;

    private EnemyPool pool;
    
    // spawnDelay = (max-min)*e^(-sd*N) + min
    public float maxSD = 1.0f;
    public float minSD = 0.1f;
    public float sdDecreasing = 0.01f;

    private float spawnCount = 0;
    private float spawnDelay = 0.0f;
    private float spawnTime = 0.0f;

	// Use this for initialization
	void Start () {
        

        pool = GetComponent<EnemyPool>();
        Restart();
	}
	
	void Update () {
        if (!isGaming)
        {
            if (Input.GetKeyUp(KeyCode.R))
            {
                Restart();
            }
            else if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        else // isGaming
        {
            // Score Processing
            score += Time.deltaTime * 100;
            SetScore((int)score);

            spawnTime -= Time.deltaTime;
            while (spawnTime <= 0.0f)
            {
                spawnTime += spawnDelay;

                spawnCount++;
                spawnDelay = (maxSD - minSD) * Mathf.Exp(-spawnCount * sdDecreasing) + minSD;
                Debug.Log("Spawn Delay = " + spawnDelay);

                Spawn();
            }
        }
	}

    public void GameOver()
    {
        explosion.transform.position = player.transform.position;

        player.SetActive(false);
        
        animExplosion = explosion.GetComponentInChildren<Animator>();
        animExplosion.SetTrigger("Start");

        isGaming = false;
        txtGameOver.enabled = true;
    }

    public void Restart()
    {
        player.transform.position = Vector3.zero;
        player.SetActive(true);

        pool.Clear();

        score = 0.0f;
        SetScore((int)score);

        isGaming = true;
        txtGameOver.enabled = false;

        spawnCount = 0;
        spawnDelay = maxSD;
        spawnTime = maxSD;
    }

    private void SetScore(int score)
    {
        txtScore.text = "Score : " + score;
    }

    private void Spawn()
    {
        Vector3 position, direction;
        //RandomPosition(out position, out direction);
        TrackPosition(out position, out direction);

        GameObject newEnemy = pool.GetEnemy();
        newEnemy.transform.position = position;

        EnemyBehavior enemyBehavior = newEnemy.GetComponent<EnemyBehavior>();
        enemyBehavior.direction = direction;

        newEnemy.SetActive(true);
    }

    private void RandomPosition(out Vector3 position, out Vector3 direction)
    {
        position = randomDirection();
        position /= Mathf.Max(Mathf.Abs(position.x), Mathf.Abs(position.y));
        position = (position + new Vector3(1.0f, 1.0f, 0.0f)) / 2;

        position = Camera.main.ViewportToWorldPoint(position);
        position.z = 0;

        direction = randomDirection();
        // if position and direction has same direction
        if (Vector3.Dot(position, direction) > 0)
        {
            direction = Quaternion.AngleAxis(180.0f, Vector3.forward) * direction;
        }
    }

    private void TrackPosition(out Vector3 position, out Vector3 direction)
    {
        position = randomDirection();
        position /= Mathf.Max(Mathf.Abs(position.x), Mathf.Abs(position.y));
        position = (position + new Vector3(1.0f, 1.0f, 0.0f)) / 2;

        position = Camera.main.ViewportToWorldPoint(position);
        position.z = 0;

        direction = (player.transform.position - position).normalized;
    }

    private static Vector3 randomDirection()
    {
        Vector3 direction = Vector3.up;
        direction = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward) * direction;
        return direction;
    }
}
