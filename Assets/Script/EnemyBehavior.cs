﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour {
    public Vector3 direction = new Vector3();
    public float speed = 20.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(direction.normalized * Time.deltaTime * speed);

        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position) - new Vector3(0.5f, 0.5f, 10.0f);
        if (Mathf.Max(Mathf.Abs(pos.x), Mathf.Abs(pos.y)) > 0.6f)
        {
            gameObject.SetActive(false);
        }
    }
}
